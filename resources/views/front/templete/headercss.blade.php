<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="xmlrpc.html" />
<!-- *********	MOBILE TOOLS	*********  -->
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<!-- *********	FAVICON TOOLS	*********  -->
<link rel="shortcut icon" href="/front/wp-content/themes/muusico/images/favicon.ico" />
<!-- <title>Muusico &#8211; Lyrics WordPress Theme</title> -->
<title>{{ APP_NAME }} @yield('title')</title>
<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel="alternate" type="application/rss+xml" title="Muusico &raquo; Feed" href="feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Muusico &raquo; Comments Feed" href="comments/feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Muusico &raquo; Homepage Comments Feed" href="homepage/feed/index.html" />
<!-- <link rel='stylesheet' id='wp-block-library-css'  href='http://www.2035themes.com/muusico/wp-includes/css/dist/block-library/style.min.css?ver=5.4.1' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='contact-form-7-css'  href='http://www.2035themes.com/muusico/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.7' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='usp_style-css'  href='http://www.2035themes.com/muusico/wp-content/plugins/user-submitted-posts/resources/usp.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='bootstrap-css'  href='http://www.2035themes.com/muusico/front//wp-content/themes/muusico/css/bootstrap.min.css?ver=1' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='all-styles-css'  href='http://www.2035themes.com/muusico/front//wp-content/themes/muusico/css/all-styles.css?ver=1' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='font-awesome-css'  href='http://www.2035themes.com/muusico/front//wp-content/themes/muusico/css/font-awesome.min.css?ver=1' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='main-css'  href='http://www.2035themes.com/muusico/front//wp-content/themes/muusico/style.css?ver=5.4.1' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='responsive-css'  href='http://www.2035themes.com/muusico/front//wp-content/themes/muusico/css/responsive.css?ver=1' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/front/wp-content/cache/wpfc-minified/1e50rso0/7n33q.css" media="all"/>
<link rel='stylesheet' id='redux-google-fonts-theme_prefix-css'  href='http://fonts.googleapis.com/css?family=PT+Sans%3A400%2C700%2C400italic%2C700italic%7COswald%3A200%2C300%2C400%2C500%2C600%2C700&amp;ver=1585146349' type='text/css' media='all' />
<script type="text/javascript">
window.ParsleyConfig = { excluded: ".exclude" };
var usp_case_sensitivity = "false";
var usp_challenge_response = 12;
</script>
<script src='/front/wp-content/cache/wpfc-minified/9k59asi1/7n33q.js' type="text/javascript"></script>
<!-- <script type='text/javascript' src='http://www.2035themes.com/muusico/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script> -->
<!-- <script type='text/javascript' src='http://www.2035themes.com/muusico/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script> -->
<!-- <script type='text/javascript' src='http://www.2035themes.com/muusico/wp-content/plugins/user-submitted-posts/resources/jquery.cookie.js'></script> -->
<!-- <script type='text/javascript' src='http://www.2035themes.com/muusico/wp-content/plugins/user-submitted-posts/resources/jquery.parsley.min.js'></script> -->
<!-- <script type='text/javascript' src='http://www.2035themes.com/muusico/wp-content/plugins/user-submitted-posts/resources/jquery.usp.core.js'></script> -->
<!-- <script type='text/javascript' src='http://www.2035themes.com/muusico/front//wp-content/themes/muusico/js/modernizr-2.6.2-respond-1.1.0.min.js?ver=5.4.1'></script> -->
<link rel='https://api.w.org/' href='/front/wp-json/index.html' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.html?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.4.1" />
<link rel="canonical" href="index.html" />
<link rel='shortlink' href='index.html' />
<link rel="alternate" type="application/json+oembed" href="/front/wp-json/oembed/1.0/embed571b.json?url=http%3A%2F%2Fwww.2035themes.com%2Fmuusico%2F" />
<link rel="alternate" type="text/xml+oembed" href="/front/wp-json/oembed/1.0/embedf4cd?url=http%3A%2F%2Fwww.2035themes.com%2Fmuusico%2F&amp;format=xml" />
<style type="text/css">
a:hover,
cite,
.post-format-icon,
var,
.big-letter h1,
.post-materials ul li i,
.post-paginate p,
.sf-menu li a:hover,
.sf-menu .menu-item-has-children:hover:before,
.lyric-alphabet ul li a:hover,
.popular-lyrics .title h3 i,
.latest-lyrics-container h3 i,
.lyric-print a,
.lyrics-title h3 a,
.same-album a {
color: #e4422e !important;
}
q,
blockquote {
border-left: #e4422e !important;
}
kbd,
.button,
.ajax-load-more,
.sidebar-widget hr,
.searchform input[type="submit"],
.post-password-form input[type="submit"],
.contact-form-style input[type="submit"],
.wpcf7 input[type="submit"],
.submit-lyric,
.media_background {
background: #e4422e !important;
}
/* Background */
body {
background: #f5f5f5 !important;
}
/* Body Font */
body {
font-family: PT Sans !important;
font-size: 14px;
line-height: 24px;
}
::-webkit-input-placeholder {
font-family: PT Sans !important;
font-size: 14px;
line-height: 24px;
}
:-moz-placeholder {
font-family: PT Sans !important;
font-size: 14px;
line-height: 24px;
}
::-moz-placeholder {
font-family: PT Sans !important;
font-size: 14px;
line-height: 24px;
}
:-ms-input-placeholder {
font-family: PT Sans !important;
font-size: 14px;
line-height: 24px;
}
/*Title Font*/
h1,
h2,
h3,
h4,
h5,
h6 {
font-family: Oswald !important;
}
</style>
<style type="text/css" title="dynamic-css" class="options-output">h2.site-description{font-family:"PT Sans";line-height:24px;font-weight:400;font-size:14px;}h2.site-description{font-family:Oswald;}</style>