<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){
        return view('front.home');
    }

    public function about(){
    	return view('front.about');
    }

    public function artists(){
    	return view('front.artists');
    }

    public function albums(){
    	return view('front.albums');
    }

    public function genres(){
    	return view('front.genres');
    }

    public function news(){
    	return view('front.news');
    }

    public function contact(){
        echo "contact page is ready";
    	// return view('front.contact');
    }

    public function test(){
        echo "this is test";
    }

}
