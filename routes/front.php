<?php

Route::group(['namespace' => 'Frontend'], function (){

	Route::get('/','PageController@index')->name('index');	
	
	Route::get('contact','PageController@contact')->name('contact');

	Route::get('test','PageController@test')->name('front-test');
});




