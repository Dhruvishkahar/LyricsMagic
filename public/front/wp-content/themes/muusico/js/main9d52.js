(function ($) {
	"use strict";


	var muusicoinit = function () {

		/* Fitvids */
		jQuery(".fitvids").fitVids();
		/* Fitvids */

	}

	$(document).ready(function () {


		$("#loadMore").on("click", tagAjaxPagination);

		function tagAjaxPagination(e) {
			e.preventDefault();
			e.stopPropagation();

			var pageNumber = $(e.target).data("page-number");
			pageNumber++;
			var tagSlug = $(e.target).data("page-type");
			var maxPost = $(e.target).data("max-page");

			$.ajax({
				method: 'POST',
				url: muusico.siteName + '/wp-admin/admin-ajax.php',

				data: {
					nonce: muusico.nonce,
					action: 'muusico_tag_ajax',
					pageId: pageNumber,
					tagSlug: tagSlug
				},
				success: function (response) {

					history.pushState({
						id: 'paged'
					}, '', muusico.siteName + "/tag/" + tagSlug + "/page/" + pageNumber);

					$(e.target).data('page-number', pageNumber);
					pageNumber++;

					$(".lyrics-list-post-content").append(response);


					if (maxPost < pageNumber) {
						$("#loadMore").remove();
					}




				},
			});

		}


		$("#loadMoreArtistLyrics").on("click", ajaxPagination);

		function ajaxPagination(e) {

			e.preventDefault();
			e.stopPropagation();

			var pageNumber = $(e.target).data("page-number");
			pageNumber++;
			var maxPost = $(e.target).data("max-page");
			var artistId = $(e.target).data('artist-id');

			$.ajax({
				method: "POST",
				url: muusico.siteName + '/wp-admin/admin-ajax.php',
				data: {
					id: pageNumber,
					artistId: artistId,
					nonce: muusico.nonce,
					action: 'muusico_single_ajax_pagination'
				},
				success: function (response) {

					history.pushState({
						id: 'paged'
					}, '', muusico.siteName + "/artist/" + muusico.artist + "/page/" + pageNumber);

					$(e.target).data('page-number', pageNumber);
					pageNumber++;

					$(".lyrics-list-post-content").append(response);


					if (maxPost < pageNumber) {
						$("#loadMoreArtistLyrics").remove();
					}

				},

			})



		}

		muusicoinit();
		$('#popular-listr').owlCarousel({
			margin: 10,
			loop: false,
			dots: false,
			nav: true,
			navText: ['', ''],
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 2
				},
				1000: {
					items: 5
				}
			}
		})

		$(".lisinput").focus(function () {
			$(".searchcats").animate({
				opacity: 1,
				bottom: "-35px"
			});
			//$(".searchcats").animate({'bottom':'-40px'});
		});

		var winHeight = $(window).height();
		var lastHeight = winHeight - 487;
		var lastHeightBlog = winHeight - 267;
		var lastHeightHome = winHeight - 575;
		$(".second-container").css("min-height", lastHeight);
		$(".blog-height").css("min-height", lastHeightBlog);
		$(".height-capsule").css("min-height", lastHeightHome);



		$(".add-artist").click(function () {
			$(".add-more-artists").css("display", "block");
			$(".add-artist").css("display", "none");
		});

		$('#usp_form a').click(function () {
			$('html, body').animate({
				scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top
			}, 500);
			return false;
		});

		$('#search-button').toggle(function (e) {
			e.preventDefault();
			e.stopPropagation();
			jQuery("#mini-search-wrapper").fadeIn();
			$(this).children("i").removeClass('fa-search');
			$(this).children("i").addClass('fa-times');
			$(this).children("i").addClass('close-search');
			$(this).children("i").removeClass('open-search');
			jQuery("#mini-search-wrapper input[type='search']").focus();
		}, function (e) {
			e.preventDefault();
			e.stopPropagation();
			jQuery("#mini-search-wrapper").fadeOut();
			$(this).children("i").removeClass('fa-times');
			$(this).children("i").addClass('fa-search');
			$(this).children("i").addClass('open-search');
			$(this).children("i").removeClass('close-search');
		});


		$('#selectize-artist').selectize({
			allowEmptyOption: true,
			create: true
		});

		$('#selectize-second-artist').selectize({
			allowEmptyOption: true,
			create: true
		});

		$('#selectize-third-artist').selectize({
			allowEmptyOption: true,
			create: true
		});

		$('#selectize-fourth-artist').selectize({
			allowEmptyOption: true,
			create: true
		});

		$('#selectize-fifth-artist').selectize({
			allowEmptyOption: true,
			create: true
		});

		$('#selectize-album').selectize({
			allowEmptyOption: true,
			create: true
		});

		/* Mobile Menu */
		$('.navigate').slicknav();
		/* Mobile Menu */

		$('.slicknav_icon').click(function () {
			$(this).toggleClass('open');
		});


	});



	jQuery(window).load(function () {


	});

})(jQuery);