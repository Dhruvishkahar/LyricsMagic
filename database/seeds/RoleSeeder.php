<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \App\Models\Role::truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        \App\Models\Role::insert(
        	[
        		['name'=>'Super Admin','created_at'=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()],
                ['name'=>'Sub Admin','created_at'=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()],

        	]
        );
    }
}
